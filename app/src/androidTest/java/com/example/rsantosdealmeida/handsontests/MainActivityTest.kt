package com.example.rsantosdealmeida.handsontests

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.core.AllOf.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    var rule
            = IntentsTestRule(MainActivity::class.java, true, true)

    @Test
    fun whenClickSend_withoutInfo_shouldShowError() {
        // Arrange

        // Action
        onView(withText("Send")).perform(click())
        // Assert
        onView(withText("Error")).check(matches(isDisplayed()))
    }

    @Test
    fun whenClickSend_withAmount_shouldShowInvalidReceiverError() {
        // Arrange
        onView(withId(R.id.amountEditText)).perform(typeText("10"))
        // Action
        onView(withText("Send")).perform(click())
        // Assert
        onView(withText("Invalid Receiver Error")).check(matches(isDisplayed()))
    }

    @Test
    fun whenClickSend_withReceiver_shouldShowInvalidAmountError() {
        // Arrange
        onView(withId(R.id.receiverEditText)).perform(typeText("test@email.com"))
        // Action
        onView(withText("Send")).perform(click())
        // Assert
        onView(withText("Invalid Amount Error")).check(matches(isDisplayed()))
    }

    @Test
    fun whenClickSend_withReceiver_andAmount_shouldOpenSentActivity() {
        intending(hasComponent(SentActivity::class.java.name))
                .respondWith(Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null))

        onView(withId(R.id.amountEditText)).perform(typeText("10"))
        onView(withId(R.id.receiverEditText)).perform(typeText("test@email.com"))

        onView(withText("Send")).perform(click())

        intended(allOf(
                hasComponent(SentActivity::class.java.name),
                hasExtra("EXTRA_AMOUNT", "10"),
                hasExtra("EXTRA_RECEIVER", "test@email.com")
        ))
    }

    @Test
    fun whenCLickOnChoose_showFillReceiver() {
        val resultIntent = Intent().putExtra("EXTRA_RECEIVER", "r.santos.de.almeida@accenture.com")
        intending(hasComponent(ChooseActivity::class.java.name))
                .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, resultIntent))

        onView(withText("Choose")).perform(click())

        onView(withText("r.santos.de.almeida@accenture.com")).check(matches(isDisplayed()))
    }
}