package com.example.rsantosdealmeida.handsontests

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendButton.setOnClickListener {
            onSendClick()
        }
        chooseButton.setOnClickListener {
            onChooseClick()
        }
    }

    private fun onSendClick() {
        val amountIsEmpty = amountEditText.text.isEmpty()
        val emailIsEmpty = receiverEditText.text.isEmpty()

        if (amountIsEmpty && emailIsEmpty) {
            errorTextView.text = "Error"
        } else if (emailIsEmpty) {
            errorTextView.text = "Invalid Receiver Error"
        } else if (amountIsEmpty) {
            errorTextView.text = "Invalid Amount Error"
        } else {
            sendAmount()
        }
        errorTextView.visibility = View.VISIBLE
    }

    private fun sendAmount() {
        val intent = Intent(this, SentActivity::class.java)
                .putExtra("EXTRA_RECEIVER", receiverEditText.text.toString())
                .putExtra("EXTRA_AMOUNT", amountEditText.text.toString())

        startActivity(intent)
    }

    private fun onChooseClick() {
        val intent = Intent(this, ChooseActivity::class.java)
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val receiver = data?.getStringExtra("EXTRA_RECEIVER")
            receiverEditText.setText(receiver)
        }
    }
}
