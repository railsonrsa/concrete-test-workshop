package com.example.rsantosdealmeida.handsontests

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class ChooseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose)
    }
}
